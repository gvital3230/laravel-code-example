<?php
/**
 * Created by PhpStorm.
 * User: vital
 * Date: 23/09/2018
 * Time: 14:48
 */

namespace App\Http\Controllers;

use App\Models\Blog;

class SiteController extends Controller
{
    public function index()
    {
        $blog = Blog::all()->first();
        if (!$blog) {
            $blog = new Blog();
        }

        return view('index', [
            'blog' => $blog
        ]);
    }
}