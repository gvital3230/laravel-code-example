<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\CrudTrait;
use Illuminate\Support\Facades\Storage;

/**
 * Class Blog
 * @package App\Models
 * @property string $name
 * @property string $body
 * @property string $front_body
 */
class Blog extends Model
{
    use CrudTrait;

    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    protected $table = 'blogs';
    protected $primaryKey = 'id';
    public $timestamps = true;
    // protected $guarded = ['id'];
    protected $fillable = [
        'name',
        'blog_category_id',
        'body'
    ];
    // protected $hidden = [];
    // protected $dates = [];

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */
    public function category()
    {
        return $this->belongsTo('App\Models\BlogCategory', 'blog_category_id', 'id');
    }

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESORS
    |--------------------------------------------------------------------------
    */
    public function getFrontBodyAttribute()
    {
        /** @var Banner $banner */
        $banner = Banner::all()->first();

        $banner_text = '';

        if ($banner) {
            $banner_text = \Html::link(
                $banner->url,
                \Html::image(
                    Storage::disk(env('ASSETS_DISK'))->url($banner->image)),
                null,
                [],
                false
            );
        }
        return str_replace(Banner::CONTENT_SHORT_TAG, $banner_text, $this->body);
    }

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}
