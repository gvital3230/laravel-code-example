@extends('layouts.app')
<?php
/**
 * @var $blog \App\Models\Blog
 */
?>

@section('content_area')

    <h1 class="mt-5">{{$blog->name}}</h1>
    <p class="lead">{!!$blog->front_body !!}</p>
@endsection
