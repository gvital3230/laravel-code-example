<!-- This file is used to store sidebar items, starting with Backpack\Base 0.9.0 -->
<li><a href="{{ backpack_url('dashboard') }}"><i class="fa fa-dashboard"></i> <span>{{ trans('backpack::base.dashboard') }}</span></a></li>
<li><a href='{{ backpack_url('banner') }}'><i class='fa fa-tag'></i> <span>Banners</span></a></li>
<li><a href='{{ backpack_url('blog') }}'><i class='fa fa-tag'></i> <span>Blogs</span></a></li>
<li><a href='{{ backpack_url('blog_category') }}'><i class='fa fa-tag'></i> <span>Blog categories</span></a></li>
